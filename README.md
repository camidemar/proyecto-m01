# Proyecto-M01


# Sóc en Pol Arnabat!
Un estudiant de cicle superior de DAW, aquest README.txt cal destacar que esta fet amb llenguatge markdown per la web stackedit.io.

# Comentar a HTML
Per comentar a HTML es fa amb la sintaxis següent:
```
<!-- INSEREIX UN COMENTARI AQUI A UN FITXER HTML -->
```
Aquesta manera, tambe serveix per XML, ja que utilitzan la mateixa sintaxis per comentar.
```
<!-- INSEREIX UN COMENTARI AQUI A UN FITXER XML -->
```

# Aquest es el meu horari de las primeras horas

| Dilluns                |Dimarts                          |Dimecres                         |Dijous | Divendres |
|----------------|-------------------------------|-----------------------------|-----------------------------|-----------------------------|
|FOL|EIE            |M01            | FOL | EIE|

## Informacio adiccional
Aquesta es una manera de fer una formula matematica a un docuement markdown. Es fa amb dos signes $$ per obrir i dos signes iguals per tencar.

$$
\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.
$$



> Aquesta es una imatge inserible al document:
> Aquesta imatge es una representació d'un rack de forma mapa conceptal
> 
![enter image description here](https://gitlab.com/polarnabat/proyecto-documentacion-para-el-administrador-de-red/-/raw/main/Images/Mapa_F%C3%ADsico_Rack__Abrir_en_formato_-Rack.drawio.png?raw=true)

## Enllaç

Aixins es com inserim un [enllaç](https://gitlab.com/polarnabat/proyecto-documentacion-para-el-administrador-de-red/-/blob/main/DOCUMENTACI%C3%93N_10ALUMNOS/README.md?ref_type=heads).
<hr>



